<?php 


session_start();
if(isset($_SESSION['admin']))
{
	$admin = $_SESSION['admin'];

?>


<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title>Blog</title>
	

		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/images/favicon.ico">

		<!-- Switchery css -->
		<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
		
		<!-- Bootstrap CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- Font Awesome CSS -->
		<link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		
		<!-- Custom CSS -->
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />		
		
		<!-- BEGIN CSS for this page -->
		<link rel="stylesheet" href="assets/plugins/trumbowyg/ui/trumbowyg.min.css">
		<!-- END CSS for this page -->
				
</head>

<body class="adminbody">

<div id="main">

    <div class="headerbar">

		<!-- LOGO -->
        <div class="headerbar-left">
			<a href="index.html" class="logo"><img alt="Logo" src="assets/images/logo.png" /> <span>Admin</span></a>
        </div>

        <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">			
		   
                       <li class="list-inline-item dropdown notif">
                            <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/avatars/admin.png" alt="Profile image" class="avatar-rounded">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Hello,  <?php echo $admin; ?></small> </h5>
                                </div>


                                <!-- item-->
                                <a href="../controller/logout.php" class="dropdown-item notify-item">
                                    <i class="fa fa-power-off"></i> <span>Logout</span>
                                </a>
								
								<!-- item-->
                           
                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left">
								<i class="fa fa-fw fa-bars"></i>
                            </button>
                        </li>                        
                    </ul>

        </nav>

	</div>
	<!-- End Navigation -->
	
 
	<!-- Left Sidebar -->
	<div class="left main-sidebar">
	
		<div class="sidebar-inner leftscroll">

			<div id="sidebar-menu">
        
			<ul>

			<li class="submenu">
						<a class="active" href="homepage.php"><i class="fa fa-fw fa-bars"></i><span> Dashboard </span> </a>
                    </li>

					<li class="submenu">
                        <a href="addblogs.php"><i class="fa fa-plus"></i><span> Add blogs </span> </a>
                    </li>
					<li class="submenu">
                        <a href="allblogs.php"><i class="fa fa-fw fa-area-chart"></i><span> All blogs </span> </a>
                    </li>
					<li class="submenu">
                        <a href="addcategory.php"><i class="fa fa-plus"></i><span> Add Category </span> </a>
                    </li>
					<li class="submenu">
                        <a href="allcategory.php"><i class="fa fa-fw fa-area-chart"></i><span> all Category </span> </a>
                    </li>
					<li class="submenu">
                        <a href="../controller/logout.php"><i class="fa fa-power-off"></i><span> Logout </span> </a>
                    </li>			
                         

					
            </ul>

      

			</div>
        
		
		</div>

	</div>
	<!-- End Sidebar -->


    <div class="content-page">
	
		<!-- Start content -->
        <div class="content">
            
			<div class="container-fluid">

					
						<div class="row">
							<div class="col-xl-12">
								<div class="breadcrumb-holder">
									<h1 class="main-title float-left">Add new blog</h1>
									<ol class="breadcrumb float-right">
									<li class="breadcrumb-item">Home</li>
									<li class="breadcrumb-item">blog</li>
									<li class="breadcrumb-item active">Add blog</li>
									</ol>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<!-- end row -->

							
						
							
							
						<div class="row">
							
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
								
								<div class="card mb-3">
										
									<div class="card-body">
								
										<form action="../model/insert_blog.php" method="post" enctype="multipart/form-data">					
										<div class="row">
									
											<div class="form-group col-xl-9 col-md-8 col-sm-12">
												<div class="form-group">
												<label>Blog title</label>
												<input class="form-control" name="title" type="text" required>
												</div>
												
												<div class="form-group">
												<label>Blogs content</label>
												<textarea rows="3" class="form-control editor" name="content"></textarea>
												</div>
												<div class="form-group">
												<label>Meta Description</label>
												<textarea rows="3" class="form-control editor" name="metades"></textarea>
												</div>
												
												<div class="form-group">
												<label>Thumbnail image</label><br />
												<input type="file" name="image">
												</div>
												
											
											</div>
											
											<div class="form-group col-xl-3 col-md-4 col-sm-12 border-left">
											

											
												
										
												
											
												<div class="form-group">
													<label>Select category</label>
													<select name="categ_id" class="form-control" required>
														
											<?php 
											include "../model/config.php";
											$sql = "select * from category;";
											$query = mysqli_query($con,$sql);
											while($row = mysqli_fetch_array($query))
											{
												
												$category = $row['category'];

								

											?>
													<option value="<?php echo $category; ?>"><?php echo $category; ?></option>
													
													<?php } 
													?>
													</select>
													
												</div>
												
											</div>
                                            <div class="form-group">
												<input type="submit" name="submit" class="btn btn-primary" value="submit">
												<input type="reset" name="reset" class="btn btn-primary" value="cancel">
												
												</div>
											</div><!-- end row -->	
										</form>
								
									</div>	
									<!-- end card-body -->								
								
								</div>
							<!-- end card -->					

							</div>
							<!-- end col -->	
														
						</div>
						<!-- end row -->	



            </div>
			<!-- END container-fluid -->

		</div>
		<!-- END content -->

    </div>
	<!-- END content-page -->
    
	<footer class="footer">
		<span class="text-right">
		Copyright <a target="_blank" href="#">Your Website</a>
		</span>
		<span class="float-right">
		Powered by Zignuts
		</span>
	</footer>

</div>
<!-- END main -->

<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/moment.min.js"></script>

<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/plugins/switchery/switchery.min.js"></script>

<!-- App js -->
<script src="assets/js/pikeadmin.js"></script>

<!-- BEGIN Java Script for this page -->
<script src="assets/plugins/trumbowyg/trumbowyg.min.js"></script>
<script src="assets/plugins/trumbowyg/plugins/upload/trumbowyg.upload.js"></script>
<script>
$(document).ready(function () {
    'use strict';
	$('.editor').trumbowyg();	
}); 
</script>
<!-- END Java Script for this page -->

</body>
</html>
<?php 
}

else
 {
    echo '<script>alert("Unauthorize Access ! first Login");</script>';
	echo "<script>window.location = 'index.php';</script>";
 } 



?>