<?php 
session_start();
if(isset($_SESSION['admin']))
{

$admin = $_SESSION['admin'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title>Blog</title>
		
	
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/images/favicon.ico">

		<!-- Bootstrap CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- Font Awesome CSS -->
		<link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		
		<!-- Custom CSS -->
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- BEGIN CSS for this page -->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
		<!-- END CSS for this page -->
		
</head>

<body class="adminbody">

<div id="main">

	<!-- top bar navigation -->
	<div class="headerbar">

		<!-- LOGO -->
        <div class="headerbar-left">
			<a href="index.html" class="logo"><img alt="Logo" src="assets/images/logo.png" /> <span>Admin</span></a>
        </div>

        <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">
						
						
                        
                        
						
                           
                           

                        <li class="list-inline-item dropdown notif">
                            <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/avatars/admin.png" alt="Profile image" class="avatar-rounded">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Hello, <?php echo $admin; ?></small> </h5>
                                </div>

                               

                                <!-- item-->
                                <a href="../controller/logout.php" class="dropdown-item notify-item">
                                    <i class="fa fa-power-off"></i> <span>Logout</span>
                                </a>
								
								<!-- item-->
                           
                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left">
								<i class="fa fa-fw fa-bars"></i>
                            </button>
                        </li>                        
                    </ul>

        </nav>

	</div>
	<!-- End Navigation -->
	
 
	<!-- Left Sidebar -->
	<div class="left main-sidebar">
	
		<div class="sidebar-inner leftscroll">

			<div id="sidebar-menu">
        
			<ul>

			<li class="submenu">
						<a class="active" href="homepage.php"><i class="fa fa-fw fa-bars"></i><span> Dashboard </span> </a>
                    </li>

					<li class="submenu">
                        <a href="addblogs.php"><i class="fa fa-plus"></i><span> Add blogs </span> </a>
                    </li>
					<li class="submenu">
                        <a href="allblogs.php"><i class="fa fa-fw fa-area-chart"></i><span> All blogs </span> </a>
                    </li>
					<li class="submenu">
                        <a href="addcategory.php"><i class="fa fa-plus"></i><span> Add Category </span> </a>
                    </li>
					<li class="submenu">
                        <a href="allcategory.php"><i class="fa fa-fw fa-area-chart"></i><span> all Category </span> </a>
                    </li>
					<li class="submenu">
                        <a href="../controller/logout.php"><i class="fa fa-power-off"></i><span> Logout </span> </a>
                    </li>	
                         

					
            </ul>

      

			</div>
        
		
		</div>

	</div>
	<!-- End Sidebar -->


    <div class="content-page">
	
		<!-- Start content -->
        <div class="content">
            
			<div class="container-fluid">
					
						<div class="row">
									<div class="col-xl-12">
											<div class="breadcrumb-holder">
													<h1 class="main-title float-left">Dashboard</h1>
													
													<div class="clearfix"></div>
											</div>
									</div>
						</div>
						<!-- end row -->

						
							<div class="row">
									<div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
											<div class="card-box noradius noborder bg-default">
													<i class="fa fa-file-text-o float-right text-white"></i>
													<h2 class="text-white text-uppercase m-b-20">blogs</h2>
													<h4 class="m-b-20 text-white counter"><?php require "../model/totalblogs.php";
													echo $row[0];
													?></h4>
												
											</div>
									</div>								

									

							</div>
							<!-- end row -->

							<div class="row">
				
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">						
									<div class="card mb-3">
										<div class="card-header">
											<h3><i class="fa fa-table"></i> All blogs</h3>
					
										</div>
											
										<div class="card-body">
											<div class="table-responsive">
											<table id="example1" class="table table-bordered table-hover display">
												<thead>
													<tr>
														<th>Id</th>
														<th>Title</th>														
														<th>Category</th>
														<th>Published Date</th>
													
													</tr>
												</thead>										
												<tbody>
													<?php 
													include "../model/config.php";
													$data = "select * from addblogs";
													$querydata = mysqli_query($con,$data);
													while($row1 = mysqli_fetch_array($querydata))
													{
													$id = $row1['id'];
													$title = $row1['title'];
													$cat = $row1['catname'];
													$date = $row1['date'];
												

													?>
															
													<tr>
														<td><?php echo $id;?></td>
														<td><?php echo $title; ?></td>
														
														<td><?php echo $cat;?></td>
														<td><?php echo $date?></td>
														
													</tr>
													<?php 	} ?>		
												</tbody>
											
											
											</table>
											</div>
											
										</div>														
									</div><!-- end card-->					
								</div>
							</div>
							
							
						
							<!-- end row -->
							
							
									



            </div>
			<!-- END container-fluid -->

		</div>
		<!-- END content -->

    </div>
	<!-- END content-page -->
    
	<footer class="footer">
		<span class="text-right">
		Copyright <a target="_blank" href="#">Your Website</a>
		</span>
		<span class="float-right">
		Powered by Zignuts
		</span>
	</footer>

</div>
<!-- END main -->

<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/moment.min.js"></script>
		
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>

<!-- App js -->
<script src="assets/js/pikeadmin.js"></script>

<!-- BEGIN Java Script for this page -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

	<!-- Counter-Up-->
	<script src="assets/plugins/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>			

	<script>
		$(document).ready(function() {
			// data-tables
			$('#example1').DataTable();
					
			// counter-up
			$('.counter').counterUp({
				delay: 10,
				time: 600
			});
		} );		
	</script>
	
	
<!-- END Java Script for this page -->

</body>
</html>

<?php 
}

else
{
   echo '<script>alert("Unauthorize Access ! first Login");</script>';
   echo "<script>window.location = 'index.php';</script>";
} 
?>