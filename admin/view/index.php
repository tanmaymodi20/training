
<html>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
  rel="stylesheet"
/>
<!-- Google Fonts -->
<link
  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
  rel="stylesheet"
/>
<!-- MDB -->
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.10.2/mdb.min.css"
  rel="stylesheet"
/>
</head>
<body>
  
    <form method="POST" action="../controller/checkadmin.php">
<section class="vh-100" style="background-color:">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card shadow-2-strong" style="border-radius: 1rem;">
          <div class="card-body p-5 text-center">

            <h3 class="mb-2">Admin login</h3>

            <div class="form-outline mb-4">
              <input type="text" name="username" id="typeuser-2" class="form-control form-control-lg" />
              <label class="form-label" for="typeuser-2">Username</label>
            </div>

            <div class="form-outline mb-4">
              <input type="password" id="typePasswordX-2" class="form-control form-control-lg" name="pwd" />
              <label class="form-label" for="typePasswordX-2">Password</label>
            </div>

           

            <input type="submit" class="btn btn-primary btn-lg btn-block" value="Login" name="submit">
                
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</form>

<!-- MDB -->
<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.10.2/mdb.min.js">
</script>
</body>
</html>
