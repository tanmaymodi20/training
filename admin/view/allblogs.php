
<?php 
session_start();
if(isset($_SESSION['admin']))
{

	$admin = $_SESSION['admin'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title>Blog</title>
	
		<!-- Favicon -->
		<link rel="shortcut icon" href="assets/images/favicon.ico">

		<!-- Switchery css -->
		<link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
		
		<!-- Bootstrap CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- Font Awesome CSS -->
		<link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		
		<!-- Custom CSS -->
		<link href="assets/css/style.css" rel="stylesheet" type="text/css" />	
		
		<!-- BEGIN CSS for this page -->

		<!-- END CSS for this page -->
				
</head>

<body class="adminbody">

<div id="main">

	<!-- top bar navigation -->
	<div class="headerbar">

		<!-- LOGO -->
        <div class="headerbar-left">
			<a href="index.html" class="logo"><img alt="Logo" src="assets/images/logo.png" /> <span>Admin</span></a>
        </div>

        <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">
						
						
                        
                        
						
                           
                           

                        <li class="list-inline-item dropdown notif">
                            <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/avatars/admin.png" alt="Profile image" class="avatar-rounded">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Hello, <?php echo $admin; ?></small> </h5>
                                </div>

                               

                                <!-- item-->
                                <a href="../controller/logout.php" class="dropdown-item notify-item">
                                    <i class="fa fa-power-off"></i> <span>Logout</span>
                                </a>
								
								<!-- item-->
                           
                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left">
								<i class="fa fa-fw fa-bars"></i>
                            </button>
                        </li>                        
                    </ul>

        </nav>

	</div>
	<!-- End Navigation -->
	
 
	<!-- Left Sidebar -->
	<!-- Left Sidebar -->
	<div class="left main-sidebar">
	
	<div class="sidebar-inner leftscroll">

		<div id="sidebar-menu">
	
		<ul>

		<li class="submenu">
						<a class="active" href="homepage.php"><i class="fa fa-fw fa-bars"></i><span> Dashboard </span> </a>
                    </li>

					<li class="submenu">
                        <a href="addblogs.php"><i class="fa fa-plus"></i><span> Add blogs </span> </a>
                    </li>
					<li class="submenu">
                        <a href="allblogs.php"><i class="fa fa-fw fa-area-chart"></i><span> All blogs </span> </a>
                    </li>
					<li class="submenu">
                        <a href="addcategory.php"><i class="fa fa-plus"></i><span> Add Category </span> </a>
                    </li>
					<li class="submenu">
                        <a href="allcategory.php"><i class="fa fa-fw fa-area-chart"></i><span> all Category </span> </a>
                    </li>
					<li class="submenu">
                        <a href="../controller/logout.php"><i class="fa fa-power-off"></i><span> Logout </span> </a>
                    </li>	
					 
					
				
		</ul>

  

		</div>
	
	
	</div>

</div>
	<!-- End Sidebar -->


    <div class="content-page">
	
		<!-- Start content -->
        <div class="content">
            
			<div class="container-fluid">

					
						<div class="row">
								<div class="col-xl-12">
									<div class="breadcrumb-holder">
										<h1 class="main-title float-left">blogs</h1>
										<ol class="breadcrumb float-right">
										<li class="breadcrumb-item">Home</li>
										<li class="breadcrumb-item active">blogs</li>
										</ol>
										<div class="clearfix"></div>
									</div>
								</div>
						</div>
						<!-- end row -->

										
						

										
										
						<div class="row">
										
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
											
									<div class="card mb-3">
									
										<div class="card-header">
										
										<h3><i class="fa fa-file-text-o"></i> All Blogs</h3>								
										</div>
										<!-- end card-header -->	
													
										<div class="card-body">
															
												<div class="table-responsive">	
												<table class="table table-bordered">
												<thead>
												  <tr>
												  
													<th>Blogs details</th>
													<th style="width:160px">Meta Description</th>
													<th style="width:160px">Category</th>
													<th style="width:100px">Actions</th>
													<th style="width:100px">slug</th>
													<th style="width:100px">date</th>
													
												  </tr>
												</thead>
												<tbody>
												<?php 
												require "../model/fetch_blog.php";

												   while($row = mysqli_fetch_array($query))
												   {
													
													$id =  $row['id'];
													$title =  $row['title'];
													 $content = $row['content'];
													 $image = $row['imageurl'];
													 $catname = $row['catname'];
													 $slug = $row['slug'];
													 $date = $row['date'];
													 $metades = $row['metades'];

												 
													   
													
													?>
													   <tr>	   				
													   <td>
													<span style="float: left; margin-right:10px;"><b><span style="padding-right:20px;"><?php echo $id; ?></span></b><img alt="image" style="max-width:140px; height:auto;" src="<?php echo '../upload/'.$image; ?>" /></span>
													   <h4> <?php echo $title; ?></h4>Published on </b> <?php echo $date; ?><br />
													   <small><?php echo $content; ?></small>
													   </td>
													   
													   
													   <td><?php echo $metades; ?></td>
													   <td><?php echo $catname; ?></td>
													   
													   <td>
														   <a href="editblog.php?id=<?php echo $id;?>" class="btn btn-primary btn-sm" data-placement="top" data-toggle="tooltip" data-title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>								
														   <a href="../model/deleteblog.php?id=<?php echo $id;?>" class="btn btn-danger btn-sm" data-placement="top" data-toggle="tooltip" data-title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
														  
													   </td>
													   <td><?php echo $slug; ?></td>
												   		<td><?php echo $date; ?></td>
													</tr>	
													<?php 
													 }
												
												
												?>
													
													
													
													</tbody>
												</table>
												</div>
																				
															
										</div>	
										<!-- end card-body -->								
											
									</div>
									<!-- end card -->					

								</div>
								<!-- end col -->	
																	
							</div>
							<!-- end row -->	



            </div>
			<!-- END container-fluid -->

		</div>
		<!-- END content -->

    </div>
	<!-- END content-page -->
	
	<footer class="footer">
		<span class="text-right">
		Copyright <a target="_blank" href="#">Your Website</a>
		</span>
		<span class="float-right">
		Powered by <b>Zignuts</b></a>
		</span>
	</footer>

</div>
<!-- END main -->

<script src="assets/js/modernizr.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/moment.min.js"></script>

<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/plugins/switchery/switchery.min.js"></script>

<!-- App js -->
<script src="assets/js/pikeadmin.js"></script>

<!-- BEGIN Java Script for this page -->

<!-- END Java Script for this page -->

</body>
</html>
<?php 

													}

else
{
   echo '<script>alert("Unauthorize Access ! first Login");</script>';
   echo "<script>window.location = 'index.php';</script>";
} 

?>