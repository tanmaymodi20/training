-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 07, 2022 at 04:05 PM
-- Server version: 5.7.37-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog1`
--

-- --------------------------------------------------------

--
-- Table structure for table `addblogs`
--

CREATE TABLE `addblogs` (
  `id` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `imageurl` varchar(255) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `metades` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `addblogs`
--

INSERT INTO `addblogs` (`id`, `title`, `content`, `imageurl`, `catname`, `slug`, `date`, `metades`) VALUES
(1, ' How to use Face ID to unlock iPhone while wearing a face mask', '<h2 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 16px; vertical-align: baseline; color: rgb(0, 0, 0); line-height: 24px; font-weight: 700; font-family: &quot;Droid Serif&quot;, serif;\">How to use Face ID with a mask (available in developer beta)</h2><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 20px; border: 0px; outline: 0px; font-size: 16px; vertical-align: baseline; color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; line-height: 28px;\">To be clear, iOS 15.4 is still not out yet. The ability to use the face unlock feature while wearing a face mask is currently in beta, and can be accessed only on the iPhone 12 series and iPhone 13 lineup. If you have access to Appleâ€™s newer iPhone with Face ID, you can install the public beta and try to use Face ID with a mask:</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 20px; border: 0px; outline: 0px; font-size: 16px; vertical-align: baseline; color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; line-height: 28px;\">1.)Open the Settings app on your iPhone.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 20px; border: 0px; outline: 0px; font-size: 16px; vertical-align: baseline; color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; line-height: 28px;\">2.) Scroll down tap Face ID and Passcode.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 20px; border: 0px; outline: 0px; font-size: 16px; vertical-align: baseline; color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; line-height: 28px;\">3.)Enter your passcode.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 20px; border: 0px; outline: 0px; font-size: 16px; vertical-align: baseline; color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; line-height: 28px;\">4.)Toggle on Use Face ID With a Mask.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 20px; border: 0px; outline: 0px; font-size: 16px; vertical-align: baseline; color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; line-height: 28px;\">5.) Next, choose the Use Face ID With a Mask option.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 20px; border: 0px; outline: 0px; font-size: 16px; vertical-align: baseline; color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; line-height: 28px;\">6.) Set up Face ID with your mask on.</p>', '985f821b9b4bbd90199fff80845744ccphone.png', 'Technology', '-How-to-use-Face-ID-to-unlock-iPhone-while-wearing-a-face-mask', '2022-02-07 05:58:00', '<p><span style=\"color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; font-size: 16px;\">Apple is aware of the situation and soon it will push out i</span><a href=\"https://indianexpress.com/article/technology/tech-news-technology/apple-universal-control-comes-to-macos-ipados-betas-ios-15-4-beta-released-7745237/\" class=\"\" style=\"padding: 0px; margin: 0px; font-size: 16px; vertical-align: baseline; background-image: initial; background-position: 0px 0px; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; outline-style: initial; outline-width: 0px; color: rgb(52, 111, 153); font-family: &quot;Droid Serif&quot;, serif;\">OS 15.4</a><span style=\"color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; font-size: 16px;\">&nbsp;in the coming days that allow iPhone owners to able to use Face ID while still wearing their masks.</span></p>'),
(2, '1000th ODI: Rohit wins toss, match, top scores as India thump Windies', '<p><span style=\"color: rgb(62, 62, 62); font-family: &quot;Droid Serif&quot;, serif; font-size: 16px;\">Sunday in Ahmedabad saw a contest between a team that had lost its last four matches across formats and another coming off a bilateral series defeat to Ireland. There were a lot of questions about the sidesâ€™ approach and efficiency in ODIs. It would be premature to suggest that India has found any answers, but their superior bench strength and the ineptitude of the West Indian batting resulted in a six-wicket win for the hosts with 22 overs to spare.</span></p>', 'be8c9724c1b4b26bcfdbb683e6f22a95sports.png', 'Sports', '1000th-ODI:-Rohit-wins-toss,-match,-top-scores-as-India-thump-Windies', '2022-02-07 06:02:39', '<h2 itemprop=\"description\" class=\"synopsis\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 20px; vertical-align: baseline; font-family: &quot;Droid Serif&quot;, serif; color: rgb(107, 107, 107); line-height: 26px;\">Sunday in Ahmedabad saw a contest between a team that had lost its last four matches across formats and another coming off a bilateral series defeat to Ireland.</h2>'),
(3, 'US airborne infantry troops in Poland amid Ukraine tension.', '<p><span style=\"color: rgb(62, 62, 62); font-family: \" droid=\"\" serif\",=\"\" serif;=\"\" font-size:=\"\" 16px;\"=\"\">Elite US troops and equipment landed Sunday in southeastern Poland near the border with Ukraine on President Joe Bidenâ€™s orders to deploy 1,700 soldiers there amid fears of a Russian invasion of Ukraine.</span><span style=\"color: rgb(62, 62, 62); font-family: \" droid=\"\" serif\",=\"\" serif;=\"\" font-size:=\"\" 16px;\"=\"\">Our national contribution here in Poland shows our solidarity with all of our allies here in Europe and, obviously, during this period of uncertainty we know that we are stronger together,â€ Donahue said at the airport.</span></p>', '52947bc4b44d92458d6e77a4bfe2b199us-news.png', 'News', 'US-airborne-infantry-troops-in-Poland-amid-Ukraine-tension.', '2022-02-07 06:10:38', '<h2 itemprop=\"description\" class=\"synopsis\" style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 20px; vertical-align: baseline; font-family: \" droid=\"\" serif\",=\"\" serif;=\"\" color:=\"\" rgb(107,=\"\" 107,=\"\" 107);=\"\" line-height:=\"\" 26px;\"=\"\">Hundreds more infantry troops of the 82nd Airborne Division are still expected to arrive at the Rzeszow-Jasionka airport.</h2>');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `pwd`) VALUES
(1, 'zignuts', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `catdes` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category`, `catdes`) VALUES
(1, 'Technology', 'tech'),
(2, 'Sports', 'sports'),
(3, 'News', 'news related');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addblogs`
--
ALTER TABLE `addblogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addblogs`
--
ALTER TABLE `addblogs`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
