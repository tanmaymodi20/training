<?php 
error_reporting(0);
      include "admin/model/config.php";
      $slug = $_GET['slug'];
      $bloginfo = "select * from addblogs where slug = '$slug'";
      $query3 = mysqli_query($con,$bloginfo);


      $getRecentlBlog = "select * from addblogs order by id DESC LIMIT 5";
      $query2 = mysqli_query($con,$getRecentlBlog);
      
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
<link rel="icon" type="image/png" href="assets/img/favicon.ico">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>Blog Assignment</title>
<link rel="stylesheet" href="home.css">
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700|Source+Sans+Pro:400,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link href="./assets/css/main.css" rel="stylesheet"/>
</head>
<body>
<nav class="topnav navbar navbar-expand-lg navbar-light bg-white fixed-top">
<div class="container">
	<a class="navbar-brand" href="index.php"><strong>Blog Assignment</strong></a>
	<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="navbar-collapse collapse" id="navbarColor02" >
		<ul class="navbar-nav mr-auto d-flex align-items-center">
			<li class="nav-item">
			<a class="nav-link" href="index.php">Home</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="index.php">Blogs</a>
			</li>
		
			
		</ul>

		
	</div>
</div>
</nav>
<!-- End Navbar -->
    
    <!--------------------------------------
HEADER

<!-- End Header -->
    
    
<!--------------------------------------
MAIN
--------------------------------------->
<div class="container pt-4 pb-4">
	<div class="row">
		<div class="col-lg-8">
  <?php 
  
  
    
      while($row1 = mysqli_fetch_array($query3))
    {
      $title = $row1['title'];
      $content = $row1['content'];
      $image = $row1['imageurl'];
      $date = $row1['date'];
      $cat = $row1['catname'];

      echo '
    <div class="row row-cols-1 row-cols-md-2 ">
      <div class="col mb-4">
        <div class="card" style="border: 1px solid black;">
          <img src="admin/upload/'.$image.'" class="img-fluid display-block p-4" style="height: 350px !important; ">
          <div class="card-body">
            <h5 class="card-title"> '.$title.'</h5>
            <p class="card-text">'.$content.'</p>
          </div>
          <div class="card-footer">
          <span class="text-dark text-muted">#'.$cat.'</span>
          <p class="card-text">'.$date.'</p>
          </div>
        </div>
      </div>
    </div>';
    }
 
  ?>
    </div>
			

		<div class="col-lg-4">
      <h3>Recent Blogs</h3>
      <?php 
  while($row1 = mysqli_fetch_array($query2))
  {
    $title = $row1['title'];
    $image = $row1['imageurl'];
    $date = $row1['date'];
    $cat = $row1['catname'];
 
  ?>
  <div class="flex-md-row mb-4 box-shadow h-xl-300">
				<div class="mb-3 d-flex align-items-center">
					<img height="80" src='<?php echo "admin/upload/".$image; ?>'>
					<div class="pl-3">
						<h2 class="mb-2 h6 font-weight-bold">
						<a class="text-dark" href="bloginfo.php?slug=<?php echo $slug;?>"><?php echo $title; ?></a>
						</h2>
						<div class="card-text text-muted small">
							 <?php echo $cat; ?>
						</div>
						<small class="text-muted"><?php echo $date; ?></small>
					</div>
				</div>
  <?php } ?>
			</div>
		</div>
	</div>
</div>
    
</div>
		</div>
<!--------------------------------------
FOOTER
--------------------------------------->
<hr style="height:1px;color:red;background-color:grey; border-width:2px;">

<div class="footer-basic">
        <footer>
            <div class="social">
            <a href="#"><i class="fab fa-instagram"></i></a>
            <a href="#"><i class="fab fa-facebook"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-google-plus-g"></i></a>
          </div>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="index.php">Home</a></li>
  
                <li class="list-inline-item"><a href="#">About</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
            </ul>
            <p class="copyright">Company Name © 2022</p>
        </footer>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>


<!-- End Footer -->
    
<!--------------------------------------
JAVASCRIPTS
--------------------------------------->
<script src="./assets/js/vendor/jquery.min.js" type="text/javascript"></script>
<script src="./assets/js/vendor/popper.min.js" type="text/javascript"></script>
<script src="./assets/js/vendor/bootstrap.min.js" type="text/javascript"></script>
<script src="./assets/js/functions.js" type="text/javascript"></script>
</body>
</html>